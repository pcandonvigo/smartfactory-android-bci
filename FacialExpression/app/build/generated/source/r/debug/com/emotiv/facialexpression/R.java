/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.emotiv.facialexpression;

public final class R {
  public static final class color {
    public static final int black_50=0x7f010000;
    public static final int color1=0x7f010001;
    public static final int color2=0x7f010002;
    public static final int color3=0x7f010003;
    public static final int color4=0x7f010004;
    public static final int colorclear=0x7f010005;
    public static final int white_80=0x7f010006;
  }
  public static final class dimen {
    public static final int activity_horizontal_margin=0x7f020000;
    public static final int activity_vertical_margin=0x7f020001;
  }
  public static final class drawable {
    public static final int bgbox=0x7f030000;
    public static final int bola=0x7f030001;
    public static final int check1=0x7f030002;
    public static final int check2=0x7f030003;
    public static final int horizentalbar=0x7f030004;
    public static final int ic_launcher=0x7f030005;
    public static final int shape=0x7f030006;
    public static final int shape2=0x7f030007;
    public static final int shape3=0x7f030008;
    public static final int shape4=0x7f030009;
    public static final int shapeclearbt=0x7f03000a;
    public static final int ticknew=0x7f03000b;
    public static final int trainbtshap=0x7f03000c;
    public static final int verticalprogressbar=0x7f03000d;
  }
  public static final class id {
    public static final int ProgressBarpower=0x7f040000;
    public static final int TextView01=0x7f040001;
    public static final int TextView02=0x7f040002;
    public static final int btClear=0x7f040003;
    public static final int btStartTrainning=0x7f040004;
    public static final int button1=0x7f040005;
    public static final int createuser=0x7f040006;
    public static final int editText1=0x7f040007;
    public static final int footer=0x7f040008;
    public static final int imageView=0x7f040009;
    public static final int imageView1=0x7f04000a;
    public static final int imgBox=0x7f04000b;
    public static final int linearLayout1=0x7f04000c;
    public static final int openSF=0x7f04000d;
    public static final int progressTimer=0x7f04000e;
    public static final int spinner1=0x7f04000f;
    public static final int spinner2=0x7f040010;
    public static final int spinner3=0x7f040011;
    public static final int textAction=0x7f040012;
    public static final int textView=0x7f040013;
    public static final int textView1=0x7f040014;
    public static final int textView2=0x7f040015;
    public static final int textView3=0x7f040016;
    public static final int viewButtom=0x7f040017;
    public static final int viewTop=0x7f040018;
    public static final int viewTop1=0x7f040019;
  }
  public static final class layout {
    public static final int activity_main=0x7f050000;
    public static final int content=0x7f050001;
    public static final int create_user=0x7f050002;
    public static final int spinner=0x7f050003;
  }
  public static final class string {
    public static final int action_settings=0x7f060000;
    public static final int app_name=0x7f060001;
    public static final int hello_world=0x7f060002;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f070000;
    public static final int AppTheme=0x7f070001;
  }
}