package com.emotiv.facialexpression;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.emotiv.bluetooth.Emotiv;
import com.emotiv.bluetooth.EmotivBluetooth;
import com.emotiv.customspinner.CustomAdapter;
import com.emotiv.customspinner.CustomSpinner;
import com.emotiv.customspinner.SpinnerModel;
import com.emotiv.dateget.EngineConnector;
import com.emotiv.dateget.EngineInterface;
import com.emotiv.sdk.*;
//import com.emotiv.insight.FacialExpressionDetection.IEE_FacialExpressionEvent_t;
//import com.emotiv.insight.FacialExpressionDetection.IEE_FacialExpressionThreshold_t;
//import com.emotiv.insight.FacialExpressionDetection.IEE_FacialExpressionTrainingControl_t;
//import com.emotiv.insight.IEmoStateDLL.IEE_FacialExpressionAlgo_t;
//import com.emotiv.insight.IEmoStateDLL.IEE_MentalCommandAction_t;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("NewApi")
public class ContentActivity extends Activity implements EngineInterface{
    Spinner spinner,spinnerSensitive;
    CustomSpinner spinner2;
    TimerTask timerTask,timerTaskAnimation;
    CustomAdapter adapter,adapterSpinnerAction,adapterSensitive;
    ImageView   imgBox;
    ProgressBar barTime,powerBar;
    Timer timer;
    boolean mapping= false;
    int indexActionSellected = 0;
	static HttpURLConnection con;
    String baseURL="http://54.38.255.176/SMARTFACTORY/api/";
    String data="";
    final String SEPARATOR ="/";

    public static String url;

    long putTime;

    float stress=0, compromise=0, interest=0, excitement=0, attention=0, relaxation=0, suitability=0, gyroX=0, gyroY=0,gyroZ=0, emg=0, eeg=0;

    float smile=0, surprise=0, furrow=0, clench=0, grimace=0;

    int station=10;

    public static boolean connected=false;



    TextView actionText;

    private Vector<String> mappingAction;
    int userId = 0,count = 0;
    EngineConnector engineConnector;
    Button btStartTrainning,btClear, btnSF;

	 public static float _currentPower = 0;
	 boolean isTrainning = false;
	 String currentRunningAction="";

	 float startLeft 	= -1;
	 float startRight 	= 0;
	 float widthScreen 	= 0;

	 public  ArrayList<SpinnerModel> CustomListViewValuesArr  = new ArrayList<SpinnerModel>();
	 public  ArrayList<SpinnerModel> CustomListViewValuesArr2 = new ArrayList<SpinnerModel>();
	 public  ArrayList<SpinnerModel> CustomListViewValuesArr3 = new ArrayList<SpinnerModel>();

	 private int counter=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		StrictMode.setThreadPolicy(policy);
		try {
			putTime=System.currentTimeMillis();
			putData();
		} catch (Exception e) {
			e.printStackTrace();
		}



		super.onCreate(savedInstanceState);
		setContentView(R.layout.content);
		engineConnector = EngineConnector.shareInstance();
		engineConnector.delegate = this;
		mappingAction = new Vector<String>();
		mappingAction.add("Neutral");
		mappingAction.add("Pull");
		mappingAction.add("Push");
		mappingAction.add("Left");
		mappingAction.add("Right");

		actionText=(TextView)findViewById(R.id.textAction);
		// get signals view
		spinner = (Spinner) this.findViewById(R.id.spinner1);
		spinner2 = (CustomSpinner) this.findViewById(R.id.spinner2);
		spinnerSensitive = (Spinner) this.findViewById(R.id.spinner3);

		barTime = (ProgressBar) this.findViewById(R.id.progressTimer);
		powerBar = (ProgressBar) this.findViewById(R.id.ProgressBarpower);
		imgBox = (ImageView) this.findViewById(R.id.imgBox);
		btStartTrainning=(Button)this.findViewById(R.id.btStartTrainning);
		btnSF = (Button)findViewById(R.id.openSF);
		btClear=(Button)this.findViewById(R.id.btClear);

		System.out.println(btnSF);
		btnSF.setOnClickListener(launchSmartFactory);
		btStartTrainning.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {

				// TODO Auto-generated method stub
				if(!engineConnector.isConnected) {
					Toast.makeText(ContentActivity.this, "You need to connect to your headset.", Toast.LENGTH_SHORT).show();
					connected=false;
				}else{
					connected=true;

					switch (indexActionSellected) {
					case 0:
						// neutral
						startTrainingFacialExpression(IEE_FacialExpressionAlgo_t.FE_NEUTRAL);
						break;
					case 1:
						//smile
						startTrainingFacialExpression(IEE_FacialExpressionAlgo_t.FE_SMILE);
						break;
					case 2:
						//clench
						startTrainingFacialExpression(IEE_FacialExpressionAlgo_t.FE_CLENCH);
						break;
					case 3:
						//frown
						startTrainingFacialExpression(IEE_FacialExpressionAlgo_t.FE_FROWN);
						break;
					case 4:
						//suprise
						startTrainingFacialExpression(IEE_FacialExpressionAlgo_t.FE_SURPRISE);
						break;
					default:
						break;
					}
				}
			}
		});
		btClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearData();
			}
		});
		barTime.setVisibility(View.INVISIBLE);
		setListData();
		Resources res = getResources();
		System.out.println("-----------------------------------------------------------Size:"+CustomListViewValuesArr.size());
		adapterSpinnerAction = new CustomAdapter(getApplicationContext(), R.layout.spinner, CustomListViewValuesArr,res);
	        // Set adapter to spinner
		spinner.setAdapter(adapterSpinnerAction);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				indexActionSellected = arg2;
				mapping = true;
				spinner2.setSelection(indexActionSellected);
 			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
		//System.out.println(CustomListViewValuesArr2.toString());
		for (SpinnerModel s :CustomListViewValuesArr) {
			System.out.println(s.getLabel());
		}
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
		adapter = new CustomAdapter(getApplicationContext(), R.layout.spinner, CustomListViewValuesArr2,res);
		adapter.headerData = mappingAction;
		spinner2.setAdapter(adapter);
		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mapping) {
					mapping = false;
					return;
				}
				mapping = true;
			   	switch (arg2){
			   		case 0:
			   		 	mappingAction.setElementAt("Neutral", indexActionSellected);
			   			break;
			   		case 1:
			   		 	mappingAction.setElementAt("Pull", indexActionSellected);
			   			break;
			   		case 2:
			   		 	mappingAction.setElementAt("Push", indexActionSellected);
			   			break;
			   		case 3:
			   			mappingAction.setElementAt("Left", indexActionSellected);
			   			break;
			   		case 4:
			   			mappingAction.setElementAt("Right", indexActionSellected);
			   			break;
			   	}

			   	spinner2.setSelection(indexActionSellected);
			   	adapter.notifyDataSetChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		adapterSensitive = new CustomAdapter(getApplicationContext(), R.layout.spinner, CustomListViewValuesArr3,res);
		spinnerSensitive.setAdapter(adapterSensitive);
		spinnerSensitive.setSelection(4);
		spinnerSensitive.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				switch (indexActionSellected) {
				case 0:
					// neutral
					edkJava.IEE_FacialExpressionSetThreshold(userId,IEE_FacialExpressionAlgo_t.FE_NEUTRAL, IEE_FacialExpressionThreshold_t.FE_SENSITIVITY, arg2*100);
					break;
				case 1:
					//smile
					edkJava.IEE_FacialExpressionSetThreshold(userId,IEE_FacialExpressionAlgo_t.FE_SMILE , IEE_FacialExpressionThreshold_t.FE_SENSITIVITY, arg2*100);
					break;
				case 2:
					//clench
					edkJava.IEE_FacialExpressionSetThreshold(userId,IEE_FacialExpressionAlgo_t.FE_CLENCH, IEE_FacialExpressionThreshold_t.FE_SENSITIVITY, arg2*100);
					break;
				case 3:
					//frown
					edkJava.IEE_FacialExpressionSetThreshold(userId,IEE_FacialExpressionAlgo_t.FE_FROWN, IEE_FacialExpressionThreshold_t.FE_SENSITIVITY, arg2*100);
					break;
				case 4:
					//suprise
					edkJava.IEE_FacialExpressionSetThreshold(userId,IEE_FacialExpressionAlgo_t.FE_SURPRISE, IEE_FacialExpressionThreshold_t.FE_SENSITIVITY, arg2*100);
					break;
				default:
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		}
		);
		Timer timerListenAction = new Timer();
		timerListenAction.scheduleAtFixedRate(new TimerTask() {
		    @Override
		    public void run() {
		    	mHandlerUpdateUI.sendEmptyMessage(1);
		    }
		},
		0, 20);
	}

	 @Override
	  public void onWindowFocusChanged(boolean hasFocus) {
		    Display display = getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			widthScreen = size.x;
			startLeft = imgBox.getLeft();
			startRight = imgBox.getRight();
	 }


	  public void setListData()
	    {

	        // Now i have taken static values by loop.
	        // For further inhancement we can take data by webservice / json / xml;
	            SpinnerModel sched = new SpinnerModel();
	            sched.setLabel("Neutral");
	            sched.setChecked(engineConnector.checkTrained(IEE_FacialExpressionAlgo_t.FE_NEUTRAL));
	            CustomListViewValuesArr.add(sched);
	            sched= new SpinnerModel();
	            sched.setLabel("Smile");
	            sched.setChecked(engineConnector.checkTrained(IEE_FacialExpressionAlgo_t.FE_SMILE));

	            CustomListViewValuesArr.add(sched);

	            sched= new SpinnerModel();
	            sched.setLabel("Clench");
	            sched.setChecked(engineConnector.checkTrained(IEE_FacialExpressionAlgo_t.FE_CLENCH));

	            CustomListViewValuesArr.add(sched);

	            sched= new SpinnerModel();
	            sched.setLabel("Frown");
	            sched.setChecked(engineConnector.checkTrained(IEE_FacialExpressionAlgo_t.FE_FROWN));
	            sched.setChecked(false);
	            CustomListViewValuesArr.add(sched);

	            sched= new SpinnerModel();
	            sched.setLabel("Surprise");
	            sched.setChecked(engineConnector.checkTrained(IEE_FacialExpressionAlgo_t.FE_SURPRISE));
	            sched.setChecked(false);
	            CustomListViewValuesArr.add(sched);
	            //////////
	            for (int i = 0 ;i < mappingAction.size() ;i ++){
	            	  sched = new SpinnerModel();
	  	            sched.setLabel(mappingAction.elementAt(i));
	  	            sched.setChecked(false);
	  	            CustomListViewValuesArr2.add(sched);
	            }
	            for(int i = 0; i < 11 ; i++){
	                //////////
		            sched = new SpinnerModel();
		            sched.setLabel("" + i);
		            CustomListViewValuesArr3.add(sched);
	            }
	}

	private void intTimerTask(){
			count=0;
		   timerTask = new TimerTask() {
			@Override
			 public void run() {
				mHandlerUpdateUI.sendEmptyMessage(0);
			 }
			};
	}
	public void startTrainingFacialExpression(IEE_FacialExpressionAlgo_t FacialExpressionAction) {
		System.out.println(FacialExpressionAction);
		try {
			isTrainning = engineConnector.startFacialExpression(isTrainning, FacialExpressionAction);
		}catch (Exception e){
			e.printStackTrace();
		}
		btStartTrainning.setText((isTrainning) ? "Abort Trainning" : "Train");
	}
	public void enableClick()
	{
		spinner.setClickable(true);
		spinner2.setClickable(true);
		btClear.setClickable(true);
		spinnerSensitive.setClickable(true);
	}
	private void runAnimation(int index,float power){
		powerBar.setProgress((int) (power*100));
		currentRunningAction = mappingAction.elementAt(index);
	}
	public void clearData(){
		switch (indexActionSellected) {
		case 0:
			// neutral
			engineConnector.trainningClear(IEE_FacialExpressionAlgo_t.FE_NEUTRAL);
			break;
		case 1:
			//smile
			engineConnector.trainningClear(IEE_FacialExpressionAlgo_t.FE_SMILE);
			break;
		case 2:
			//clench
			engineConnector.trainningClear(IEE_FacialExpressionAlgo_t.FE_CLENCH);
			break;
		case 3:
			//furrow
			engineConnector.trainningClear(IEE_FacialExpressionAlgo_t.FE_FROWN);
			break;
		case 4:
			//furrow
			engineConnector.trainningClear(IEE_FacialExpressionAlgo_t.FE_SURPRISE);
			break;
		default:
			break;
		}
	}

	// mark to engine interface
	@Override
	public void userAdded(int userId) {
		// TODO Auto-generated method stub
		this.userId=userId;
	};
	@Override
	public void detectedActionLowerFace(IEE_FacialExpressionAlgo_t typeAction, float power) {
		// TODO Auto-generated method stub
		_currentPower=power*10;

		loadData(typeAction.name(),_currentPower);


		try {
			putData();
		} catch (Exception e) {
			e.printStackTrace();
		}



		actionText.setText("Action: "+typeAction+ "\n Power: " + power);

		/*if (typeAction == IEE_FacialExpressionAlgo_t.FE_SMILE) {
			runAnimation(indexActionSellected,power);
		}
		else if (typeAction == IEE_FacialExpressionAlgo_t.FE_CLENCH)
			runAnimation(indexActionSellected,power);*/
	}

	@Override
	public void userRemove() {
		this.userId=-1;
		// TODO Auto-generated method stub
	}
	@Override
	public void trainStarted() {
		// TODO Auto-generated method stub
		   barTime.setVisibility(View.VISIBLE);
		   spinner.setClickable(false);
		   spinner2.setClickable(false);
		   btClear.setClickable(false);
		   spinnerSensitive.setClickable(false);
		   timer = new Timer();
		   intTimerTask();
		   timer.schedule(timerTask ,0, 10);
	}
	@Override
	public void trainSucceed() {
		// TODO Auto-generated method stub
		barTime.setVisibility(View.INVISIBLE);
		enableClick();
		btStartTrainning.setText("Train");
		new AlertDialog.Builder(this)
	    .setTitle("Training Succeeded")
	    .setMessage("Training is successful. Accept this training?")
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	     public void onClick(DialogInterface dialog, int which) {
	            // continue with delete
	        	engineConnector.setTrainControl(IEE_FacialExpressionTrainingControl_t.FE_ACCEPT);
	        }
	     })
	    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            // do nothing
	        	engineConnector.setTrainControl(IEE_FacialExpressionTrainingControl_t.FE_REJECT);
	        }
	     })
	    .setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
	}


	@Override
	public void trainCompleted() {
		// TODO Auto-generated method stub
		SpinnerModel model = CustomListViewValuesArr.get(indexActionSellected);
		model.setChecked(true);
		CustomListViewValuesArr.set(indexActionSellected, model);
        adapterSpinnerAction.notifyDataSetChanged();

        new AlertDialog.Builder(this)
	    .setTitle("Training Completed")
	    .setMessage("")
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	     public void onClick(DialogInterface dialog, int which) {
	            // continue with delete
	        }
	     })
	    .setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
		isTrainning = false;
	}

	@Override
	public void trainRejected() {
		// TODO Auto-generated method stub
		SpinnerModel model = CustomListViewValuesArr.get(indexActionSellected);
		model.setChecked(false);
		enableClick();
		isTrainning=false;
	}

	@Override
	public void trainErased() {
		// TODO Auto-generated method stub
		   new AlertDialog.Builder(this)
		    .setTitle("Training Erased")
		    .setMessage("")
		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
		     public void onClick(DialogInterface dialog, int which) {
		            // continue with delete
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert)
		     .show();

		SpinnerModel model = CustomListViewValuesArr.get(indexActionSellected);
		model.setChecked(false);
		CustomListViewValuesArr.set(indexActionSellected, model);
		adapterSpinnerAction.notifyDataSetChanged();
		enableClick();
		isTrainning=false;
	}
	@Override
	public void trainReset() {
		// TODO Auto-generated method stub
		if(timer!=null){
			timer.cancel();
			timerTask.cancel();
			barTime.setVisibility(View.INVISIBLE);
			powerBar.setProgress(0);
		}
		enableClick();
		isTrainning=false;
	}

	/// handler
    public Handler mHandlerUpdateUI = new Handler() {
	       public void handleMessage(Message msg) {
	        switch (msg.what) {
			case 0:
				count ++;
				SWIGTYPE_p_unsigned_int pValue = edkJava.new_uint_p();
				edkJava.IEE_FacialExpressionGetTrainingTime(userId, pValue);
				int trainningTime=(int)edkJava.uint_p_value(pValue);
				edkJava.delete_uint_p(pValue);
				trainningTime = trainningTime/1000;
				if(trainningTime > 0)
					barTime.setProgress(count / trainningTime);
				if (barTime.getProgress() > 100) {
					timerTask.cancel();
					timer.cancel();
				}
				break;
			case 1:
				moveImage();
				break;

			default:
				break;
			}
	       }
	    };
	    private void moveImage() {
			float power = _currentPower;
			if(isTrainning){
				imgBox.setLeft((int)(startLeft));
				imgBox.setRight((int) startRight);
				imgBox.setScaleX(1.0f);
				imgBox.setScaleY(1.0f);
			}
			if(( currentRunningAction.equals("Neutral"))  || (currentRunningAction.equals("Right")) && power > 0) {

				if(imgBox.getScaleX() == 1.0f && startLeft > 0) {

					imgBox.setRight((int) widthScreen);
					power = ( currentRunningAction.equals("Left")) ? power*3 : power*-3;
					imgBox.setLeft((int) (power > 0 ? Math.max(0, (int)(imgBox.getLeft() - power)) : Math.min(widthScreen - imgBox.getMeasuredWidth(), (int)(imgBox.getLeft() - power))));
				}
			}
			else if(imgBox.getLeft() != startLeft && startLeft > 0){
				power = (imgBox.getLeft() > startLeft) ? 6 : -6;
				imgBox.setLeft(power > 0  ? Math.max((int)startLeft, (int)(imgBox.getLeft() - power)) : Math.min((int)startLeft, (int)(imgBox.getLeft() - power)));
			}
			if((( currentRunningAction.equals("Pull")) || ( currentRunningAction.equals("Push"))) && power > 0) {
				if(imgBox.getLeft() != startLeft)
					return;
				imgBox.setRight((int) startRight);
				power = (currentRunningAction.equals("Push")) ? power / 20 : power/-20;
				imgBox.setScaleX((float) (power > 0 ? Math.max(0.1, (imgBox.getScaleX() - power)) : Math.min(2, (imgBox.getScaleX() - power))));
				imgBox.setScaleY((float) (power > 0 ? Math.max(0.1, (imgBox.getScaleY() - power)) : Math.min(2, (imgBox.getScaleY() - power))));
			}
			else if(imgBox.getScaleX() != 1.0f){
				power = (imgBox.getScaleX() < 1.0f) ? 0.03f : -0.03f;
				imgBox.setScaleX((float) (power > 0 ? Math.min(1, (imgBox.getScaleX() + power)) : Math.max(1, (imgBox.getScaleX() + power))));
				imgBox.setScaleY((float) (power > 0 ? Math.min(1, (imgBox.getScaleY() + power)) : Math.max(1, (imgBox.getScaleY() + power))));
			}
		}

	public void onBackPressed() {
	    android.os.Process.killProcess(android.os.Process.myPid());
		finish();
	}

	public void putData()throws Exception{
	    if(System.currentTimeMillis()-putTime<2000 || smile==0.0 || attention==20.0)
	    	return;

	    putTime=System.currentTimeMillis();

		url = baseURL + "operadores/updateBCI/1/"+ stress + SEPARATOR + compromise + SEPARATOR + interest + SEPARATOR
					+ excitement + SEPARATOR + attention + SEPARATOR + relaxation + SEPARATOR + suitability + SEPARATOR
					+ eeg + SEPARATOR + emg + SEPARATOR + gyroX + SEPARATOR
					+ gyroY + SEPARATOR + gyroZ + SEPARATOR;
		System.out.println("PUT Data: "+url);
		try{
			URL myurl=new URL(url);
			con= (HttpURLConnection) myurl.openConnection();

			con.setRequestMethod("GET");

			con.getInputStream();

		}catch (Exception e){
			data+="ERROR "+e.toString()+"\n";
			e.printStackTrace();
		}finally{
			con.disconnect();
		}

		smile=0;
		furrow=0;
		surprise=0;
		grimace=0;


	}

	public void getData() throws Exception{
		String url=baseURL+"getOperadores/Operador estacion 10";
		try {

			URL myurl = new URL(url);
			con = (HttpURLConnection) myurl.openConnection();

			con.setRequestMethod("GET");

			StringBuilder content;

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String line;
			content = new StringBuilder();

			if ((line = in.readLine()) != null) {
				content.append(line);
				content.append(System.lineSeparator());
				data+=line+"\n";
			}


		}catch (Exception e){
			data+="ERROR "+e.toString()+"\n";
			Log.d("ERROR",e.toString());
		} finally {

			con.disconnect();
		}
		data=data.replaceAll("\\[","").replaceAll("\\]","");

		String fields[]=data.split(",");

		suitability=Float.parseFloat(fields[1]);
		stress=Float.parseFloat(fields[4]);
		compromise=Float.parseFloat(fields[5]);
		interest=Float.parseFloat(fields[6]);
		excitement=Float.parseFloat(fields[7]);
		attention=Float.parseFloat(fields[8]);
		relaxation=Float.parseFloat(fields[9]);
		gyroX=Float.parseFloat(fields[13]);
		gyroY=Float.parseFloat(fields[14]);
	}

	void loadData(String pAction, float pValue){

		switch (pAction) {
			case "FE_SMILE": smile=pValue;
				break;
			case "FE_CLENCH": clench=pValue;
				break;
			case "FE_FROWN": furrow=pValue;
				break;
			case "FE_SURPRISE": surprise=pValue;
				break;
			default:
				break;
		}

		//Emotiv.IEE_EmoInitDevice(this);


		//edkJava.IEE_EngineConnect("Emotiv Systems-5");
		//IEE_MotionDataChannel_t[] ChannelTmp = {IEE_MotionDataChannel_t.IMD_GYROX,IEE_MotionDataChannel_t.IMD_GYROY};

		attention = (float) (2*smile + 3*surprise + furrow - 2.5 * grimace);
		compromise = (float) (smile + 2*surprise -2*furrow -1.5 * grimace);
		interest = (float) (2*smile+3*surprise- 2*furrow- 1.5*grimace);
		excitement = (float) (-3*smile + 2*surprise + 3*furrow + 5*grimace);
		stress = (float) (-2*smile + 3*surprise + 5*furrow + 10* grimace);
		relaxation = (float) (2*smile - 6*surprise -5*furrow -9*grimace);
		suitability = (float) (100+2*attention + 3*compromise + 3*interest -2*excitement -4*stress + relaxation);

	}

	private OnClickListener launchSmartFactory = new OnClickListener()
	{

		public void onClick(View v)
		{
			try {
				Intent launchSF = getPackageManager().getLaunchIntentForPackage("com.Altran.SmartFactory");
				startActivity(launchSF);
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	};

}
